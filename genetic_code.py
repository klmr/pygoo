#!/usr/bin/env python

"""
The universal genetic code.

This module provides helper functions for working with (universal) genetic code,
such as translating codons into amino acids or iterating over all possible
codons.
"""

import string
import itertools
import os.path

_datafilename = os.path.join(os.path.dirname(__file__), 'genetic_code.dat')

dna_alphabet = 'ACGT'
rna_alphabet = 'ACGU'
default_alphabet = dna_alphabet

def codons(alphabet = default_alphabet):
    """
    Enumerate all possible codons for a given ``alphabet``.
    """
    return map(lambda c: ''.join(c), itertools.product(alphabet, repeat = 3))


def empty_codon_dict(alphabet = default_alphabet):
    """
    Return a dictionary of counts for all possible codons for a given ``alphabet``.
    """
    the_codons = codons(alphabet)
    return dict(zip(the_codons, [0] * len(the_codons)))


def _load_genetic_code(datafile):
    """
    Load the genetic code from a file object.

    Each line in the file represents one codon in the format
    
        codon amino_acid

    i.e. codon, followed by whitespace, followed by amino acid in one-letter
    code.
    """
    return dict(tuple(line.split()) for line in datafile)


def _patch_code(code, alphabet):
    table = string.maketrans(default_alphabet, alphabet)

    if alphabet == default_alphabet:
        return code
    if isinstance(code, str):
        return code.translate(table)

    return dict((key.translate(table), val) for key, val in code.items())


class UnknownNucleotideAlphabetError(Exception):
    def __init__(self, alphabet):
        self.alphabet = alphabet

    def __str__(self):
        return '%s is not a known nucleotide alphabet' % self.alphabet


def _reverse_alphabet(alphabet):
    if alphabet == dna_alphabet:
        return 'TGCA'
    elif alphabet == rna_alphabet:
        return 'UGCA'
    else:
        raise UnknownNucleotideAlphabetError(alphabet)


class _genetic_code:
    def __init__(self, alphabet, datafilename):
        datafile = open(datafilename, 'r')
        starts = datafile.readline().strip().split(' ')[1:]
        self.start = set(_patch_code(start, alphabet) for start in starts)
        self.code = _patch_code(_load_genetic_code(datafile), alphabet)
        self.stop = set(codon for codon, acid in self.code.items() if acid == 'Stop')
        # Generate all possible codons since data file may not be exhaustive.
        self.codons = codons(alphabet)
        # This ignores (for now, but intentionally) selenocystein
        self.amino_acids = list('ARNDCEQGHILKMFPSTWYV')
        self.rctab = string.maketrans(alphabet, _reverse_alphabet(alphabet))

    def revcomp(self, seq):
        """
        Return the reverse complement of ``sequence``
        """
        return seq.translate(self.rctab)[::-1]

    def translate(self, codons):
        # Strip off trailing stop codon, if present
        if codons[-3 : ] in self.stop:
            codons = codons[ : -3]
        return ''.join(
                self.code[codons[i * 3 : (i + 1) * 3]]
                    for i in range(len(codons) // 3))


def make_genetic_code(alphabet = default_alphabet, filename = _datafilename):
    return _genetic_code(alphabet, filename)

genetic_code = make_genetic_code()
