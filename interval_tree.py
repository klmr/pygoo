"""
Interval tree implementation

Description: http://en.wikipedia.org/wiki/Interval_tree
Adapted from http://forrst.com/posts/Interval_Tree_implementation_in_python-e0K
and made slightly more sane to use (= intervals are just tuples).
"""

class IntervalTree:
    def __init__(self, intervals):
        self.top_node = self._divide_intervals(intervals)

    def _divide_intervals(self, intervals):
        if not intervals:
            return None

        x_center = self._center(intervals)

        s_center = []
        s_left = []
        s_right = []

        for k in intervals:
            if k[1] < x_center:
                s_left.append(k)
            elif k[0] > x_center:
                s_right.append(k)
            else:
                s_center.append(k)

        return Node(x_center, s_center, self._divide_intervals(s_left), self._divide_intervals(s_right))


    def _center(self, intervals):
        fs = _sort_by_begin(intervals)
        length = len(fs)

        return fs[int(length/2)][0]

    def search(self, begin, end=None):
        if end:
            result = []

            for j in xrange(begin, end+1):
                for k in self.search(j):
                    result.append(k)
                result = list(set(result))
            return _sort_by_begin(result)
        else:
            return self._search(self.top_node, begin, [])

    def _search(self, node, point, result):
        for k in node.s_center:
            if k[0] <= point <= k[1]:
                result.append(k)
        if point < node.x_center and node.left_node:
            for k in self._search(node.left_node, point, []):
                result.append(k)
        if point > node.x_center and node.right_node:
            for k in self._search(node.right_node, point, []):
                result.append(k)

        return list(set(result))


class _Node:
    def __init__(self, x_center, s_center, left_node, right_node):
        self.x_center = x_center
        self.s_center = _sort_by_begin(s_center)
        self.left_node = left_node
        self.right_node = right_node


def _sort_by_begin(intervals):
    return sorted(intervals, key=lambda x: x[0])
