The `goo`
=========

This repository contains bioinformatics related code snippets and modules.
It is for use in other projects and resides in the local `PYTHONPATH`.

License
-------

Unless otherwise noted, released under GPL v3. See `LICENSE` for details.
